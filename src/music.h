#define Do      262
#define DoD     277
#define Re      294
#define ReD     311
#define Mi      330
#define Fa      349
#define FaD     370
#define Sol     392
#define SolD    415
#define La      440
#define LaD     466
#define Si      494

#define TAILLE_PARTITION 500
#define TITRE_PARTITION 64

typedef struct {
    char titre[TITRE_PARTITION];
    int sons[TAILLE_PARTITION][3];
} partition;