#include <stdio.h>
#include <math.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"
#include "systick.h"
#include "adc.h"
#include "music.h"

void tim2_pwm_PB9_init();
void playNote(uint32_t note);
void playNoteOctave(uint32_t note, uint8_t octave);
void setVolume(uint32_t volume);

static volatile uint8_t vol=1;
static volatile uint32_t cpt = 0;
static volatile uint32_t flag = 0;

void __attribute__((interrupt)) SysTick_Handler(){
    if(cpt==10){
        cpt=0;
        flag=1;
    }
    cpt++;
}

int main() {
  
  printf("\e[2J\e[1;1H\r\n*** Welcome to Nucleo F446 ! ***\r\n");
  printf("   %08lx-%08lx-%08lx\r\n",U_ID[0],U_ID[1],U_ID[2]);
  printf("SYSCLK = %9lu Hz\r\n",get_SYSCLK());
  printf("AHBCLK = %9lu Hz\r\n",get_AHBCLK());
  printf("APB1CLK= %9lu Hz\r\n",get_APB1CLK());
  printf("APB2CLK= %9lu Hz\r\n",get_APB2CLK());
  printf("\r\n");

    configuration_potentiometre_classique();
    SysTick_init(1000);
    tim2_pwm_PB9_init();


    /*
    printf("Play a note (Specify a frequency).\n");
    while(1){
        scanf("%ld",&freq);
        printf("%ld\n",freq);
        playNote(freq,vol);
    }
    */

    playNoteOctave(La,3);

    uint32_t mesure;

    do{
        while(flag==0) __WFI();
        mesure = mesure_potentiometre_classique()*100/4096; /*Regle de 3. On garde seulement les 2 lsb*/
        setVolume(mesure);
        flag=0;
    }while(1);

    return 0;
}



void playNote(uint32_t note){
  TIM2.ARR = (get_APB1TIMCLK()/TIM2.PSC)/note; //Hz
  TIM2.EGR |= 1; //Generates an UEV to reset counter and update the autoreload register. Note play instantly
}

void playNoteOctave(uint32_t note, uint8_t octave){
    if(octave<3){
        playNote(note>>(3-octave));
    }else if(octave>3){
        playNote(note<<(octave-3));
    }else{
        playNote(note);
    }
}

void setVolume(uint32_t volume){
      TIM2.CCR2 = TIM2.ARR*volume/2/100; // [1-100] /2 Because it decreases after 50%;
}

void tim2_pwm_PB9_init(){
  enable_GPIOB();
  GPIOB.MODER |= (0b10 << 18); //Alternate function PB9
  GPIOB.AFRH |= (1 << 4); //AF1
	
	//RCC.APB1ENR |= (1 << 0); //Enable clock access to tim2
  enable_TIM2();

  TIM2.PSC = 84; //Pre scaler 168
  TIM2.CNT = 0; //Counter set to 0
  TIM2.CCMR1 |= (0b110 << 12); // PWM mode 1
  TIM2.CCER |= (1<<4); //Enable output on AF1
  TIM2.CR1 |= 1; //Enable timer
}

